package fake

import (
	"context"
	"fmt"
	"net/http"
	"sync/atomic"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/policy"
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/runtime"
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/compute/armcompute/v4"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/network/armnetwork/v2"
)

type Instance struct {
	InstanceId        string
	ProvisioningState string
	InstanceStatus    []string
}

type Client struct {
	Name           string
	TargetCapacity int
	Instances      []Instance

	count atomic.Uint64
}

func New() *Client {
	return &Client{}
}

func (c *Client) GetScaleSetVM(ctx context.Context, resourceGroupName string, vmScaleSetName string, instanceID string, options *armcompute.VirtualMachineScaleSetVMsClientGetOptions) (armcompute.VirtualMachineScaleSetVMsClientGetResponse, error) {
	for _, instance := range c.Instances {
		if instance.InstanceId == instanceID {

			vm := armcompute.VirtualMachineScaleSetVM{
				Location:   to.Ptr("test-location"),
				InstanceID: to.Ptr(instance.InstanceId),
			}

			return armcompute.VirtualMachineScaleSetVMsClientGetResponse{
				VirtualMachineScaleSetVM: vm,
			}, nil
		}
	}

	return armcompute.VirtualMachineScaleSetVMsClientGetResponse{}, fmt.Errorf("instance not found")
}

func (c *Client) GetScaleSet(ctx context.Context, resourceGroupName string, vmScaleSetName string, options *armcompute.VirtualMachineScaleSetsClientGetOptions) (armcompute.VirtualMachineScaleSetsClientGetResponse, error) {
	set := armcompute.VirtualMachineScaleSet{
		Location: to.Ptr("test-location"),
		SKU: &armcompute.SKU{
			Capacity: to.Ptr(int64(c.TargetCapacity)),
		},
	}

	return armcompute.VirtualMachineScaleSetsClientGetResponse{VirtualMachineScaleSet: set}, nil
}

type NopPoller[T any] struct {
	Out *T
}

func NewNopPoller[T any](result *T) *NopPoller[T] {
	nop := &NopPoller[T]{}
	nop.Out = result
	return nop
}

func (nop *NopPoller[T]) Done() bool {
	return true
}

func (nop *NopPoller[T]) Poll(context.Context) (*http.Response, error) {
	return &http.Response{}, nil
}

func (nop *NopPoller[T]) Result(ctx context.Context, out *T) error {
	*out = *nop.Out
	return nil
}

func (c *Client) BeginDeleteInstances(ctx context.Context, resourceGroupName string, vmScaleSetName string, vmInstanceIDs armcompute.VirtualMachineScaleSetVMInstanceRequiredIDs, options *armcompute.VirtualMachineScaleSetsClientBeginDeleteInstancesOptions) (*runtime.Poller[armcompute.VirtualMachineScaleSetsClientDeleteInstancesResponse], error) {
	for idx, instance := range c.Instances {
		found := false
		for _, id := range vmInstanceIDs.InstanceIDs {
			if *id == instance.InstanceId {
				found = true
				break
			}
		}
		if !found {
			continue
		}

		c.Instances[idx].ProvisioningState = "Deleting"
		c.Instances[idx].InstanceStatus = nil
	}

	opts := &runtime.NewPollerOptions[armcompute.VirtualMachineScaleSetsClientDeleteInstancesResponse]{}
	opts.Response = &armcompute.VirtualMachineScaleSetsClientDeleteInstancesResponse{}
	opts.Handler = NewNopPoller(opts.Response)

	poller, err := runtime.NewPoller[armcompute.VirtualMachineScaleSetsClientDeleteInstancesResponse](&http.Response{}, runtime.NewPipeline("", "", runtime.PipelineOptions{}, &policy.ClientOptions{}), opts)
	if err != nil {
		return nil, err
	}

	return poller, nil
}

func (c *Client) BeginUpdate(ctx context.Context, resourceGroupName string, vmScaleSetName string, parameters armcompute.VirtualMachineScaleSetUpdate, options *armcompute.VirtualMachineScaleSetsClientBeginUpdateOptions) (*runtime.Poller[armcompute.VirtualMachineScaleSetsClientUpdateResponse], error) {
	c.TargetCapacity = int(*parameters.SKU.Capacity)

	for len(c.Instances) < c.TargetCapacity {
		c.Instances = append(c.Instances, Instance{
			InstanceId:        fmt.Sprintf("instance__%d", c.count.Add(1)),
			ProvisioningState: "Succeeded",
			InstanceStatus:    []string{"PowerState/running"},
		})
	}

	set := armcompute.VirtualMachineScaleSet{}

	opts := &runtime.NewPollerOptions[armcompute.VirtualMachineScaleSetsClientUpdateResponse]{}
	opts.Response = &armcompute.VirtualMachineScaleSetsClientUpdateResponse{VirtualMachineScaleSet: set}
	opts.Handler = NewNopPoller(opts.Response)

	poller, err := runtime.NewPoller[armcompute.VirtualMachineScaleSetsClientUpdateResponse](&http.Response{}, runtime.NewPipeline("", "", runtime.PipelineOptions{}, &policy.ClientOptions{}), opts)
	if err != nil {
		return nil, err
	}

	return poller, nil
}

func (c *Client) NewListPager(resourceGroupName string, virtualMachineScaleSetName string, options *armcompute.VirtualMachineScaleSetVMsClientListOptions) *runtime.Pager[armcompute.VirtualMachineScaleSetVMsClientListResponse] {
	result := armcompute.VirtualMachineScaleSetVMListResult{}

	for idx, instance := range c.Instances {
		if instance.ProvisioningState == "Deleting" {
			c.Instances = append(c.Instances[:idx], c.Instances[idx+1:]...)
			c.TargetCapacity--
		}
	}

	for _, instance := range c.Instances {
		var statuses []*armcompute.InstanceViewStatus

		for _, status := range instance.InstanceStatus {
			statuses = append(statuses, &armcompute.InstanceViewStatus{Code: to.Ptr(status)})
		}

		result.Value = append(result.Value, &armcompute.VirtualMachineScaleSetVM{
			InstanceID: to.Ptr(instance.InstanceId),
			Properties: &armcompute.VirtualMachineScaleSetVMProperties{
				ProvisioningState: to.Ptr(instance.ProvisioningState),
				InstanceView: &armcompute.VirtualMachineScaleSetVMInstanceView{
					Statuses: statuses,
				},
			},
		})
	}

	handler := runtime.PagingHandler[armcompute.VirtualMachineScaleSetVMsClientListResponse]{
		More: func(armcompute.VirtualMachineScaleSetVMsClientListResponse) bool {
			return false
		},

		Fetcher: func(ctx context.Context, vmssvclr *armcompute.VirtualMachineScaleSetVMsClientListResponse) (armcompute.VirtualMachineScaleSetVMsClientListResponse, error) {
			return armcompute.VirtualMachineScaleSetVMsClientListResponse{
				VirtualMachineScaleSetVMListResult: result,
			}, nil
		},
	}

	return runtime.NewPager(handler)
}

func (c *Client) GetVirtualMachineScaleSetNetworkInterface(ctx context.Context, resourceGroupName string, virtualMachineScaleSetName string, virtualmachineIndex string, networkInterfaceName string, options *armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceOptions) (armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceResponse, error) {
	inter := armnetwork.Interface{}

	return armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceResponse{Interface: inter}, nil
}
